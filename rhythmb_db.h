#ifndef RHYTHMB_DB_H
#define RHYTHMB_DB_H

#include "rhytmb_client.h"

#define PATH_TO_DATABASE "/.local/share/rhythmbox/rhythmdb.xml"
//#define PATH_TO_DATABASE "/rhytmb_test.xml"

class Rhythmb_db
{
   ifstream inp_;
   Rhytmb_client client_;
   string full_path_to_deleted_file_;
 public:
    explicit Rhythmb_db(const Rhytmb_client &rhythm);
   ~Rhythmb_db();


    void convert_lines_to_db_format();
    string decode_url_line(const string &src);
    char *read_line();
    int delete_file();
    int compare(const char *first_inp, const string &second);
    int compare(const char *first_inp, const char *second);

    int compare_year(const char *first_inp, const string &second);
    string dec_to_hex_string(unsigned int input);

    int check_existence_of_file(const string &line);


  private:
    int search_for_file(string &buf_location);
    int start_position(const char *line);
    string remove_special_symbols_to_code_point(const string &input);
    void remove_special_symbols_from_code_point(string &input);
    int location_path_check(const string &line);
    string convert_location_path_to_utf8(const string &input);
    void correct_moving_of_file_to_trash(const string &path);

};

#endif // RHYTHMB_DB_H
