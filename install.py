# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 10:18:58 2013

@author: gekannt@ya.ru
"""
import os,re, time
from os.path import expanduser

CONST_NAME_OF_FILE_WITH_SETUPS=".xbindkeysrc"
CONST_NAME_RESERVED_FOR_THIS_UTILITY="# !3@fnjfndk05fgfkdlmsasdklclmsadksdmkdcdos - secret cipher of our combination. Don't change it, else program won't be able to clean up and will mess in this file"
PATH_TO_FILE=""
def check_file(): # maybe it's bad that everything after us in the file will be deleted
                 # but nothing should be there according to its format, I do so because it's simpler to implement
    global PATH_TO_FILE                 
    home=expanduser("~")
    home=home+"/.xbindkeysrc"
    PATH_TO_FILE = home
    print home
    f=open(home,"rw+")
    storage=""    
    
    for line in f:
     storage=storage+line    # storing content of file in variable
     line=line[:-1] # deleting the newline symbol
     if (line==CONST_NAME_RESERVED_FOR_THIS_UTILITY):
            
      f.close()
      f=open(home,"w")
      f.write(storage)
      f.close()
      
      return True
    
    f.close()
    return False  # there are no our records yet in the file
    

    

def get_parameters():
    f=os.popen("xbindkeys -k") 
    parameters = f.read()
    
    p = re.compile(r'\n')
    strings= p.split(parameters)
    #for line in strings:
        #print line

#    print  strings[-2], strings[-3]
    return strings[-2],strings[-3]
    #print "now is",now

def installation_of_necessary_components():
    TIME_WAIT=4
    print "command \"sudo apt-get install xbindkeys trash-cli g++  qt4-qmake  make\" will be executed"
    time.sleep(TIME_WAIT)
    os.system("sudo apt-get install xbindkeys trash-cli g++ qt4-qmake make")
  
    home=expanduser("~")
    home=home+"/.xbindkeysrc"  
    

    try:
        f=open(home,"r")
        f.close()
    except IOError:        
        print "command \"xbindkeys --defaults > ~/.xbindkeysrc\" will be executed"
        time.sleep(TIME_WAIT)
        os.system("xbindkeys --defaults > ~/.xbindkeysrc")    
        
    print "programm will be compiled"
    time.sleep(TIME_WAIT)    
    os.system("make")
    
    print "programm will be installed"
    time.sleep(TIME_WAIT)        
    os.system("sudo cp -f rhythmbox_deleter /usr/bin/rhythmbox_deleter")
    


def main():
     installation_of_necessary_components()
     lines=get_parameters()
     print lines[1],"\n", lines[0]
     #exit(0)
     if (check_file()==True ):
         f=open(PATH_TO_FILE,"a")    # our  CONST_NAME_RESERVED_FOFFFR_THIS_UTILITY was found there
         f.write("\n\"/usr/bin/rhythmbox_deleter\"\n")
         f.write(lines[1])
         f.write("\n#")
         f.write(lines[0])
         f.write("\n#")
         f.close()   
     else:
         f=open(PATH_TO_FILE,"a")
         f.write(CONST_NAME_RESERVED_FOR_THIS_UTILITY)
         f.write("\n\"/usr/bin/rhythmbox_deleter\"\n")
         f.write(lines[1])
         f.write("\n#")
         f.write(lines[0])
         f.write("\n#")
         f.close()     
     
     os.system("killall xbindkeys")  #  faster way than previous command
     os.system("xbindkeys ")
  #   os.system("sudo reboot")
#     os.system("killall -HUP xbindkeys") # reloading configuration file
 #    os.system("rm  *.o")
     print "if xbindkeys doesn't react at any presses of keys,rebooting may be needed"
     
if __name__ == "__main__":
    main()