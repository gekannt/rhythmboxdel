#include "rhythmb_db.h"


Rhythmb_db::Rhythmb_db(const Rhytmb_client &rhythm) : client_(rhythm)
{
    char *s = getenv("HOME");
    if ( s == NULL) {
        cerr<<"not possible to get HOME variable";
        INFO
        exit(-1);
    }

   string  full_path_to_db = s;
    full_path_to_db += PATH_TO_DATABASE;

    inp_.open(full_path_to_db.c_str());
    if ( !inp_.is_open()) {
        cerr<<"not possible to open the database"; INFO;
        exit(-1);
    }
}



Rhythmb_db::~Rhythmb_db() {
    inp_.close();
}

void Rhythmb_db::correct_moving_of_file_to_trash(const string &path) {
    string command = "trash-put ";
    command+='"' + path + '"';
    int ret_val =  system(command.c_str());
    cerr<<"\n"<<ret_val<<"\n";
    if(  ret_val != 0) { // trash-put failed to move file to a bin

        system(" notify-send 'Проблема' 'Deletion failed' --icon=dialog-information");
//     remove(path.c_str());
//      cerr<<"trash-put wasn't found and  file was just erased"; INFO;
    } else {
        system(" notify-send 'Song has been deleted' 'successful' --icon=dialog-information");
    }

}

int Rhythmb_db::check_existence_of_file(const string &line) {
    FILE *f= fopen(line.c_str(),"r");
    if ( f == NULL) {
        return false;
    }
    fclose(f);
    return true;
}

int Rhythmb_db::delete_file() {

  string path_to_file;
  inp_.seekg(0,inp_.beg);

  bool ret_val_exist = false;

  while (ret_val_exist != true) {
    if ( search_for_file(path_to_file) == true) {

//        remove(full_path_to_deleted_file_.c_str())];
      path_to_file = convert_location_path_to_utf8(path_to_file);

      if( ( ret_val_exist = check_existence_of_file(path_to_file) )  == false )
          continue;

      correct_moving_of_file_to_trash(path_to_file);

    } else {
        cerr<<full_path_to_deleted_file_<<"  file wasn't found\n";
        system(" notify-send 'Проблема' 'Deletion failed, we are sorry' --icon=dialog-information");
        break;
    }

  }

}

char *Rhythmb_db::read_line() {
    static char line[MAX_LENGTH_OF_INFO*2];
    // vulnerability
    inp_.getline(line,MAX_LENGTH_OF_INFO*2);
    return line;
}

int Rhythmb_db::start_position(const char *line) {
    int  i=0, len = strlen(line);
    for( i=0;i<len, line[i]==' '; i++)  // omitting spaces
        ;
    return i;
}


int Rhythmb_db::compare(const char *first_inp, const string &second) {

    if( second.empty() ) // this field is empty in db, so it shouldn't be compared
        return true;  // and it coincides by default

    int i = start_position(first_inp);
    string first(first_inp+i,first_inp+strlen(first_inp));

    if ( first.length() == second.length() &&
         !memcmp(first.c_str(),second.c_str(),first.length()) ) {
         return true;
    }

  return false;
}


int Rhythmb_db::compare_year(const char *first_inp, const string &second) {

  if( second.empty() ) {
     return true; // this field is absent
  }

  int j = start_position(first_inp),
          l = strlen(first_inp);

  int i = 0;
  for (i=j; i< l; i++)
      if (first_inp[i]=='>')
          break;

  const char *line_start = "<date>";
  if ( memcmp(first_inp+j,line_start,strlen(line_start)) )
      return false; //not that field

   i++;

   int k = 0;
   for( k = i; k<l; k++)
       if ( first_inp[k] == '<')
           break;

   string first(first_inp+i,first_inp+k);
   #define NUMBER_DAYS_IN_ONE_YEAR 365
   int numb = stoi (first) / NUMBER_DAYS_IN_ONE_YEAR;
   cout<<first<<"\n";
   string ready= "<date>";
   ready += to_string(numb);
   ready += "</date>";

   if ( ready == second )
     return true;
   return false;
}


int Rhythmb_db::compare(const char *first_inp, const char *second) {

    int a = start_position(first_inp);
    int b = start_position(second);

    if ( strlen(first_inp)-a == strlen(second)-b &&
         !memcmp(first_inp+a,second+b,min(strlen(first_inp)-a,strlen(second)-b) ))
         return true;
   return false;
}


string Rhythmb_db::remove_special_symbols_to_code_point(const string &input) {
    int l = input.length();
    string ret;
    for (int i= 0; i< l; i++ ) {
        if ( input[i]!='&')
            ret+=input[i];
        else
            ret+="&amp;";
    }
   return ret;
}


void Rhythmb_db::remove_special_symbols_from_code_point(string &input) {

  const char *sample_ampersand = "&amp;";
  string::iterator iter;

  while (true) { // loop  of replacing symbols &amp; with &
      iter = search(input.begin(),input.end(),sample_ampersand,sample_ampersand+strlen(sample_ampersand));
      if ( iter == input.end() )
          break;

      iter++;
      input.erase(iter,iter+strlen(sample_ampersand)-1); // & sign must be left, so -1 for saving it

  }
}



string Rhythmb_db::convert_location_path_to_utf8(const string &input) {

    const char *sample_start = "<location>file://",
               *sample_end =  "</location>";

//    char data[800] = {0, };
//    memcpy(data,input.data(),input.length());
    int len = input.length();

//     string::iterator iter;

    auto iter_start = search(input.begin(),input.end(),sample_start,sample_start+strlen(sample_start)),
            iter_end = search(input.begin(),input.end(),sample_end,sample_end+strlen(sample_end));

    if ( iter_start == input.end() || iter_end == input.end())  {
        cerr<<"wrong location line"<<"  "<<input<<endl;
        INFO;
        exit(-1);
    }

    iter_start+= strlen(sample_start);

    string line_clean(iter_start,iter_end);
//    char path[line_clean.length()];
        char path[MAX_LENGTH_OF_INFO] = {0, };
//    memset(path,0,line_clean.length());

   int j_path =0;

    for( int i =0; i< len; i++ ) {
        if ( line_clean[i]!='%')
//            ret += line_clean[i];
            path[j_path++] = line_clean[i];
        else {
            string sym;

            sym +=line_clean[++i];
            sym+= line_clean[++i];

            stringstream stream;
            unsigned number = 0;
            stream << std::hex << sym;
            stream >> number;
            path[j_path++]= number;
//            cout<<"here";
        }
    }

    string ret(path,path+strlen(path));


    remove_special_symbols_from_code_point(ret);
    cout<<"\npath before opening  "<<path<<"\n";


    FILE *f = fopen(ret.c_str(),"r");

    if (f == NULL) {
        cout<<"\n\n\can't be opened\n\n";
    }
    else {
        cout<<"\nopened\n\n";
        fclose(f);
    }
    return ret;
}

string Rhythmb_db::dec_to_hex_string(unsigned int input) {
    string ret;
    const int BASE_SYSTEM = 16;

    while(true) {
      unsigned a = input % BASE_SYSTEM;
      switch (a) {
      case 10:
          ret.insert(0,"A");
          break;
      case 11:
          ret.insert(0,"B");
          break;
      case 12:
          ret.insert(0,"C");
          break;
      case 13:
          ret.insert(0,"D");
          break;
      case 14:
          ret.insert(0,"E");
          break;
      case 15:
          ret.insert(0,"F");
          break;
      default:
          ret.insert(0,to_string(a));
          break;
      }
      input = input / BASE_SYSTEM;
      if (input == 0)
          break;
    }
    return  ret;
}

/* this funciton is not finised, parsing of 3 and 4 bytes UTF-8 string isn't done  */
void Rhythmb_db::convert_lines_to_db_format() {

    for(int i=0; i< NUMBER_OF_LINES_FROM_DB; i++) { // replacing @ with special symbol
        client_.song_data_[i] = remove_special_symbols_to_code_point(client_.song_data_[i]);
    }

  string line;
    for(int i=0; i <NUMBER_OF_LINES_FROM_DB; i++)  {
        line = client_.get_ith_field(i);
        int len =  client_.get_ith_field(i).length();

        string res;
        unsigned char m[len];
        memset(m,0,len);

        memcpy(m,client_.get_ith_field(i).c_str(),client_.get_ith_field(i).length());


        for( int j =0; j<len; j++ ) {
            if (m[j] < 0x80)    // 1 byte, nothing special
                res+=m[j];
            else
                if (m[j] <= 0xDF) { // 2 bytes
                  unsigned char high = 0, low = 0, middle = 0, high_part = m[j];
                  high = m[j];
                  //~~  reset three highest bits
                  high = high << 3;
                  high = high >> 3;
                  // ~~
                  j++;
                  low = m[j];
                  low = low << 2;
                  low = low >> 2;

                  middle = high << 6;
                  low = low | middle;
                  high = high >> 2;

                  unsigned int  full = high;
                  full = full << 8;
                  full = full + low;
                  res+="&#x" + dec_to_hex_string(full) + ';';

                }
            else
                    if (m[j] < 0xE0) { // 3 bytes

                        // another code, which is not written yet
                        cout<<"done";
            }
            else if ( m[j] == 0xF)   { // 4 bytes

            }



              //if   4 bytes  - it should be written too

        }
        client_.song_data_[i] = res; // reparsed line

    }

}


/*
<entry type="song">
  <title>Es Brennt (Bonus)</title>
  <genre>Rock</genre>
  <artist>Megaherz</artist>
  <album>Herzwerk II</album>
  <track-number>14</track-number>
  <duration>242</duration>
  <file-size>9713664</file-size>
  <location>file:///home/sh/music/Megaherz%20-%20Complete%20Discography%20%5B1995%20-%202008%5D%20MP3%20320kbps%20CBR%20RPTT/2002%20-%20Herzwerk%20II/Megaherz_Herzwerk%20II_14_Es%20Brennt%20(Bonus).mp3</location>
  <mtime>1373729702</mtime>
  <first-seen>1376526002</first-seen>
  <last-seen>1377722260</last-seen>
  <play-count>1</play-count>
  <last-played>1377617091</last-played>
  <bitrate>320</bitrate>
  <date>730851</date>
  <media-type>audio/mpeg</media-type>
  <album-artist>Megaherz</album-artist>
</entry>
*/

int Rhythmb_db::location_path_check(const string &line) {
   const char *SEARCHED_LINE ="<location>";
    if ( strlen(SEARCHED_LINE) >= line.length() )
        return false;

    if ( search(line.begin(),line.end(),SEARCHED_LINE,SEARCHED_LINE+strlen(SEARCHED_LINE)) != line.end())
        return true;
    return  false;
}


int Rhythmb_db::search_for_file(string &buf_location) {


  char *begin_entry =  "<entry type=\"song\">",
       *end_entry="</entry>";

  char *line;
  int counter_to_get = 0;
//  string buf_location;
  while ( !inp_.eof()) { // general reading of file

      line = read_line();
//      string c= decode_url_line(v);
      if ( compare(begin_entry,line)  == true ) {
//         string line_searched =
        counter_to_get = 0;

        //begin was found, let's look until the end of entry
          while (true) {

              line = read_line();
              if ( compare(line,end_entry) == true ) // end of entry
                  break;

              if (location_path_check(line) == true)
                buf_location = line;


//              string line_from_store = client_.get_next_string_to_compare(counter_to_get);
              if ( counter_to_get !=4 && compare(line,client_.get_next_string_to_compare(counter_to_get)) == true
                   || counter_to_get == 4 && compare_year(line,client_.get_next_string_to_compare(counter_to_get)) )                                                                                                                                                                                                                                                     {
//                   string found = client_.get_next_string_to_compare(counter_to_get);
                             counter_to_get++;
                             // reading the rest of lines, perhaps location is not read yet
                             if (counter_to_get == NUMBER_OF_LINES_FROM_DB -1) {
                                 while(true) {
                                     line =read_line();
                                     if ( compare(line,end_entry) == true ) // end of entry
                                         break;
                                     if (location_path_check(line) == true)
                                         buf_location = line;
                                 }

                                 break;
                             }

              }
          }

          if (counter_to_get == NUMBER_OF_LINES_FROM_DB-1) { // full coincidence
              cout<<buf_location<<"\n";
              return true;
          }
      }

  }
  cout<<"not found\n";
  return false;
}




string Rhythmb_db::decode_url_line(const string &SRC) {
    #define PERCENT_SYMBOL 37
           string ret;  char ch;
           int i, ii;
           for (i=0; i<SRC.length(); i++) {
               if (int(SRC[i]) == PERCENT_SYMBOL) {
                   sscanf(SRC.substr(i+1,2).c_str(), "%x", &ii);
                   ch=static_cast<char>(ii);
                   ret+=ch;
                   i=i+2;
               } else {
                   ret+=SRC[i];
               }
           }
   return (ret);
}
