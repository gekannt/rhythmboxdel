#include "inclusion.h"

#include "rhytmb_client.h"
#include "rhythmb_db.h"


// rhythmbox-client --print-playing-format=%td' '%tt' '%ta' '%tn' '%ay' '%at

// track duration -       <duration>276</duration>   - 4:36 - in seconds
// track title   -     <title>Das Tier</title>
// track artist -      <artist>Megaherz</artist>
// track number  -     <track-number>2</track-number>
// track year    -  <date>/365 - year
// album title -   <album>

#define LOG_FILE_STDERR "/tmp/stderr_rhythmbox_send_me_plz_to_____contact@alexshulzhenko.ru"
#define LOG_FILE_STDOUT "/tmp/stdout_rhythmbox_send_me_plz_to_____contact@alexshulzhenko.ru"


void redirect() {
    if(  freopen(LOG_FILE_STDERR,"a",stderr) ==  NULL ) {
        cerr<<"stderr can't  be redirected\n";
        exit(-1);
    }

    if (  freopen(LOG_FILE_STDOUT,"a",stdout) == NULL ) {
        cerr<<"stdout can't be redirected\n";
        exit(-1);
    }
    cerr<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
    cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
}

int main()
{
 #ifndef DEBUG
//   redirect();
 #endif

   Rhytmb_client client;
   client.parse_playing_song();
//   client.show();
   cout<< client.get_track_duration()<<"\n";
   cout<< client.get_track_title()<<"\n";
   cout<< client.get_track_artist()<<"\n";
//   cout<< client.get_track_number()<<"\n";
   cout<< client.get_album_year()<<"\n";
   cout<< client.get_album_title()<<"\n";

   Rhythmb_db database(client);
   database.convert_lines_to_db_format();
   database.delete_file();

   // list to next song
  #ifndef DEBUG
//   system("rhythmbox-client --next");l
  #endif
   return 0;
}
