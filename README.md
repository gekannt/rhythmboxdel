Simple program to  remove the current playing song from well-known Rhythmbox music player for Linux. 


More information can be found  at  [link](http://www.alexshulzhenko.ru/rhythmbox-delete-current-song)