#ifndef DATA_RB_H
#define DATA_RB_H
#include "inclusion.h"

//#define LINE_COMMAND "rhythmbox-client --print-playing-format=%td'\n'%tt'\n'%ta'\n'%tn'\n'%ay'\n'%at'\n'%aa > "
#define LINE_COMMAND "rhythmbox-client --print-playing-format=%td'\n'%tt'\n'%ta'\n'%ay'\n'%at'\n'%aa > "
#define PATH_FILE "/tmp/path.rhythm"
#define NUMBER_OF_LINES_FROM_DB 5

#define TRACK_DURATION 0
#define TRACK_TITLE 1
#define TRACK_ARTIST 2
#define TRACK_NUMBER 3
#define ALBUM_TITLE 4
#define ALBUM_YEAR 3

class Rhythmb_db;

class Rhytmb_client{
 friend class Rhythmb_db;

 public:
    Rhytmb_client();
    int parse_playing_song();

    string &get_track_duration();
    string &get_track_title();
    string &get_track_artist();
//    string &get_track_number();
    string &get_album_title();
    string &get_album_year();

    string &get_next_string_to_compare(int i_counter_line);
    string &get_ith_field(int i);
    void show();

 private:
    vector<string> song_data_;

};

#endif // DATA_RB_H
