#-------------------------------------------------
#
# Project created by QtCreator 2012-01-02T10:29:51
#
#-------------------------------------------------

QT       -= core

QT       -= gui

TARGET = rhythmbox_deleter
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    rhythmb_db.cpp \
    rhytmb_client.cpp

HEADERS += \
    inclusion.h \
    rhythmb_db.h \
    rhytmb_client.h

QMAKE_CXXFLAGS += "-std=c++11"
CXXFLAGS="-std=c++0x"
