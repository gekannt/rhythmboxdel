#include "rhytmb_client.h"

Rhytmb_client::Rhytmb_client() {

}

string &Rhytmb_client::get_next_string_to_compare(int i_counter_line) {
    static string ret;
//    ret.clear();

    if(i_counter_line > 5 || i_counter_line < 0) {
        cerr<<"breaking of bounds\n";
        INFO; exit(-1);
    }

    switch (i_counter_line) {
    case 0:
        ret = get_track_title();
        break;
    case 1:
       ret = get_track_artist();
       break;
    case 2:
        ret = get_album_title();
        break;
//    case 3:
//        ret = get_track_number();
//        break;
    case 3:
        ret = get_track_duration();
        break;
    case 4:
        ret = get_album_year();
        break;

    default:
        break;
    }

    return ret;
}

string &Rhytmb_client::get_ith_field(int i) {
    return song_data_[i];
}

int Rhytmb_client::parse_playing_song()
{
 string s = LINE_COMMAND;
 s+=PATH_FILE;

 //@@@
 if(system(s.c_str())==-1) // it should be better replaced with exec() function
 {  fprintf(stderr,"%s","system command failed");  exit(-2); }

 ifstream inp;
 inp.open(PATH_FILE,ios_base::in);

 if ( !inp.is_open() ) { cerr<<"error opening file"<<__FILE__"  "<<__LINE__<<"\n"; exit(-1); }

 char line[MAX_LENGTH_OF_INFO];

 for(int i=0;i<NUMBER_OF_LINES_FROM_DB; i++) {
     inp.getline(line,MAX_LENGTH_OF_INFO);
     song_data_.push_back(line);
 }

 inp.close();
}

void Rhytmb_client::show() {
    for(int i=0; i<NUMBER_OF_LINES_FROM_DB; i++) {
        cout<<song_data_[i]<<" ";
    }
    cout<<endl;
}

string &Rhytmb_client::get_track_duration() {
    static string ret;
//    ret.clear();

  if(!song_data_[TRACK_DURATION].empty() )  {
//    string::iterator iter;
   int found = song_data_[TRACK_DURATION].find(':');
   if ( found == string::npos) {
       cerr<<"wrong position,time line\n";
       cerr<<"initiative error line is  "<<song_data_[TRACK_DURATION]<<"\n";
       exit(-1);
   }

   string minutes (song_data_[TRACK_DURATION],0,found),
           seconds(song_data_[TRACK_DURATION],found+1,song_data_[TRACK_DURATION].length());

   #define SECONDS_IN_ONE_MINUTE 60
   int t = stoi( minutes) * SECONDS_IN_ONE_MINUTE + stoi (seconds);
           //minutes+':'+seconds;
   ret = "<duration>";
   ret += to_string(t);

   ret+="</duration>";
  } else {
//      ret = "<duration></duration>";
//      cerr<<"unknown type standart of database"<<endl;
//      INFO;  exit(-1);
      ret.empty();
  }

  return ret;
}


string &Rhytmb_client::get_track_title() {
    static string ret;
//    ret.clear();

//    const char *s = song_data_[TRACK_TITLE].data();
//    const wchar_t *ws = (const wchar_t *) song_data_[TRACK_TITLE].data();

    if ( ! song_data_[TRACK_TITLE].empty()) {
     ret = "<title>";
     ret+= song_data_[TRACK_TITLE];
     ret+="</title>";
    } else {
        //        ret =  "<title></title>";
//        cerr<<"unknown type standart of database";
//        INFO;  exit(-1);
        ret.empty();
    }

   return ret;
}


string &Rhytmb_client::get_track_artist() {
    static string ret;
//    ret.clear();

//    const char *s = song_data_[TRACK_ARTIST].data();
//    const wchar_t *ws = (const wchar_t *) song_data_[TRACK_ARTIST].data();

    if (!song_data_[TRACK_ARTIST].empty()) {
     ret ="<artist>";
     ret += song_data_[TRACK_ARTIST];
     ret +="</artist>";
    } else {
//        ret = "<artist></artist>";
//        cerr<<"unknown type standart of database"<<endl;
//        INFO;  exit(-1);
        ret.empty();
    }
    return ret;
}

// !!! this field should be checked LAST!!!
// is saved not in format of database like 731216, but rather 2008
// it should be used for giving a more precise definition whether it's exactly that track
string &Rhytmb_client::get_album_year() {
    static string ret;
//    ret.clear();

    string look = song_data_[ALBUM_YEAR];

    if(!song_data_[ALBUM_YEAR].empty() ) {
      ret = "<date>";
      ret +=song_data_[ALBUM_YEAR];
      ret +="</date>";
    } else {
//        ret = "<date></date>";
//        cerr<<"unknown type standart of database"<<endl;
//        INFO;  exit(-1);
        ret.empty();
    }

    return  ret;
}

string &Rhytmb_client::get_album_title() {
    static string ret;
//    ret.clear();

    if ( !song_data_[ALBUM_TITLE].empty()) {
      ret ="<album>";
      ret += song_data_[ALBUM_TITLE];
      ret +="</album>";
    } else {
//        ret = "<album></album>";
//        cerr<<"unknown type standart of database"<<endl;
//        INFO;  exit(-1);
        ret.empty();
    }

    return ret;
}
