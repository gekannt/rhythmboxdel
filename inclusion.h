/*
 trash-cli must be installed
   with a command
 sudo apt-get install trash-cli
*/

// - PLUGIN FOR DEBUGGING
//#define DEBUG

#ifndef INCLUSION_H
#define INCLUSION_H

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <unistd.h>

#include <cctype>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;


//#define DEBUG

#define INFO cerr<<"\n"<<__FILE__<<" "<<__LINE__<<endl;

#define PATH_FILE "/tmp/path.rhythm"
#define DEFAULT_LOCATION "/home/music"
#define MAX_LENGTH_OF_INFO 20000

#define TRASH "/home/sh/.local/share/Trash/files/"

#endif // INCLUSION_H
